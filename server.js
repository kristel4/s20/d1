const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const viewersRoutes = require('./routes/Viewer');

const app = express();

app.use(cors());

//connect to mongodb atlas via mongoose
mongoose.connect('mongodb+srv://admin:KMna4711@wdc028-course-booking.pbprs.mongodb.net/dbBooking_app?retryWrites=true&w=majority', {
	useNewUrlParser: true,
	useUnifiedTopology: true,
	useFindAndModify: false
}); //as is true & false


//set notification for connection success or failure
let db = mongoose.connection;
//if connection error encountered, output it in console
db.on('error', (console.error.bind(console, 'connection error: ')));

//once connected, show notif
db.once('open', () => console.log("We're connected to our database."));

//Middleware - built-in function ng app
app.use(express.json()); //binabato ng client
app.use(express.urlencoded( {extended:true})); //allows other data type like boolean aside from strings

//define scheme - structure of docs

const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: 'pending'
	}
})
const Task = mongoose.model('Task', taskSchema);

//user define taskSchema
//new - gagawin ni js, icocopy nya kay mongoose at istore nya sa new task
const userSchema = new mongoose.Schema({
	username: String,
	email: String,
	password: String,
	tasks: [
		taskSchema
	]
})

const User = mongoose.model('User', userSchema);
// task user define due to MVC = model(blueprints), views(user interface), controllers(logic ex: validation)

//register a new user
app.post('/user', (req, res)=>{
	//check if username or email are duplicate prior to registration
	User.find({
		$or: [{ username: req.body.username },
			{ email: req.body.email }] }, (findErr, duplicates) => {
			//if an error encountered during registration, display notif
			if(findErr)
				return console.error(findErr);
			//display notif if duplicates
			if(duplicates.length > 0) {
				return res.status(403).json({
					message: "Duplicate found, kindly choose different username and/or email."
				})
			} else {
			//instantiate a new user object with properties derived from the request body
			let newUser = new User({
				username: req.body.username,
				email: req.body.email,
				password: req.body.password,
				//task is initially an empty array
				task: []
			})
			//save
			newUser.save((saveErr, newUser) => {
				//if an error was encountered while saving the document - notif in the console
				if(saveErr)
					return console.error(saveErr);
				return 
					res.status(201).json({
						message: `User ${newUser.username} successfully registered,`,
						data: {
							username: newUser.username,
							email: newUser.email,
							link_to_self: `/user/${newUser._id}`
						}
				});
			})
		}
	})
});
//display user
app.get('/user/:id', (req, res) => {
	User.findById(req.params.id, (err, user) => {
		if(err) return console.error(err);
		return res.status(200).json({
			message: "User retrieved successfully.",
			data: {
				username: user.username,
				email: user.email,
				tasks: `/user/${user._id}/tasks`
			}
		})
	})
})


//Routes
app.use('/', viewersRoutes); //'/viewers' - user define or kahit wala laman

//create port -communication
const port = 3000;
app.listen(port, () => {console.log(`Listening on port ${port}.`)})